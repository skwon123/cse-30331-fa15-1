Homework 2 - Part B
===================
Due 9/17 at the beginning of the class

3. Please give the worst-case time complexity for each of the sorting algorithms below:

-  Selection sort: O( )
-  Insertion sort: O( )
-  Heap sort: O( )



4. Insertion sort is faster than selection sort when the input array is already in sorted order.
A. True
B. False
C. It depends


