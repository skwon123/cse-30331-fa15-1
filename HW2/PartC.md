Homework 2 - Part C
===================
DUE 9/22 at the beginning of the class

5. What are the three main steps in the quicksort method? 


6. Which of these statements is/are NOT true of mergesort and its variations? More than one statement may be false.
A. Merge sort is normally only used for external sorting. 
B. Merge sort is usually implemented recursively. 
C. The worst-case performance for merge sort is O(n log n).
D. Merge sort is especially useful on files that have a size that is a power of 2.