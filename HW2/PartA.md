Homework 2 - Part A
===================
Due 2015/09/15 at the beginning of class.
 

1. In the first step of inserting a new entry into a binary heap, where is the new entry initially placed? Why?





2. Suppose top is called on a priority queue that has exactly two entries with equal priority. How is the return value of top selected?
A. The implementation gets to choose either one.
B. The one which was inserted first.
C. The one which was inserted most recently.
D. This can never happen (violates the precondition).