Homework 5 - Part B
===================
Due 2015/11/12 at the beginning of class.


Q3: If you are doing a BFS in a graph with n vertices, where n > 1, then how large can the queue become?



Q4: Given two vertices in a graph s and t, which of the two traversals (BFS and DFS) can be used to find if there is path from s to t?
    A.BFS
    B.DFS
    C.both BFS and DFS
    D.neither BFS or DFS
