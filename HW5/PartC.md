Homework 5 - Part C
===================
Due 2015/11/17 at the beginning of class.

Q5: Which data structure is usually used for implementing Dijkstra’s shortest path algorithm?
    A.priority queue
    B.stack
    C.queue
    D.hash table


Q6: In Dijkstra’s algorithm, what is the initial value of the distance of each node (other than the starting vertex)?