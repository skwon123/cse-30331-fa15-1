Homework 4 - Part A
===================
Due 2015/10/27 at the beginning of class.

Q1. What are the average cost for lookup, insertion and deletion in a hash table? Express your answer using big O notation in terms of n, the number of elements in the table. Assume that the hash function is uniform and the load factor (alpha) is small.

Lookup:
Insertion:
Deletion:


Q2. Suppose I have a hash table with ten buckets (0-9) using separate chaining. The hash function is h(x) = x % 10. I insert the following integers into the hash table: 55, 39, 10, 25. What will the hash table look like?