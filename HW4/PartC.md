Homework 4 - Part C
===================
Due 2015/11/03 at the beginning of class.

Q5. [Note: clarified 2015/10/31] If I'm trying to sort a bunch of values between 0 and 999,999,999 inclusive, using bucket sort with 1000 buckets, which of these is a valid hash function to use? Assume that / means integer division.
x % 1000
x % 1000000
x / 1000
x / 1000000



Q6. Briefly explain how it is possible for bucket sort to be faster than O(n log n) when O(n log n) is the best sorting algorithm we’ve seen previously.
