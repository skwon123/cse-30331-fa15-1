Milestone 1
===========

Due: 2015/09/10 at 11:59pm
Points: 2

1a. List the names of your team members (2-4 members including yourself).

1b. Write down your top three choices for open-source software
projects to work on. Each can be one of the pre-built projects or
something else; in the latter case, provide a short paragraph
explaining your choice.
