Milestone 5
===========

Due: 2015/11/19 at 11:59pm
Points: 2

5a. Discuss how you are measuring the performance of your
modifications. For example:

- Inserting code to output time stamps
- Using vmstat and valgrind to study the RAM requirements
- We will compile with profiler flags and run the code through Intel's vtune.

5b. What results have you gotten so far? If you have run into any
problems, please describe them.
