Homework 1 - Part B
===================

Due 2015/09/03 at the beginning of class.
(For more submission questions, please refer to PartA)

1. In a few words, summarize the advantages of each of: singly-linked lists, doubly-linked lists, and circularly-linked lists.

2. Where are the template parameters of a template visible?

    (a) inside the block that contains `template<...>`
    (b) inside the function/class that immediately follows `template<...>`
    (c) the whole program
    (d) all of the above

  
