Homework 3 - Part C
===================
Due 2015/10/06 at the beginning of class.

Q5: What other data structure is a red-black tree equivalent to?


Q6: What is the worst-case running time of searching for an element of a red-black tree? Use big-O in terms of n, the number of elements in the tree.



